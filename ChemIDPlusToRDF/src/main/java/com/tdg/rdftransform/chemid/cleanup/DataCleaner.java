package com.tdg.rdftransform.chemid.cleanup;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.tdg.rdftransform.chemid.common.Utils;
import com.tdg.rdftransform.chemid.inputhandler.ChemicalData;
 
public class DataCleaner {

		private static DataCleaner instance = null;
		private static Set<String> classificationCodesTobeBlocked = null;
		private static Set<String> namesTobeBlocked = null;

		private DataCleaner() throws Exception {
			classificationCodesTobeBlocked = getCodesToBeBlocked();
			namesTobeBlocked = getNamesToBeBlocked();
		}
		
		public static DataCleaner getInstance() throws Exception {
			if (instance == null)
				instance = new DataCleaner();
			return instance;
		}
		
		/** Cleanup systematic names, prefNames and Classification codes*/
		public ChemicalData getCleanedUpData(ChemicalData input) {
			ChemicalData output = new ChemicalData();
			
			if (input.getUniqueId().equals("000050033"))
				System.out.println("Got handle");
			
			output = new ChemicalData(input);

			if (Utils.hasContent(input.getPrefName()) && namesTobeBlocked.contains(input.getPrefName().toLowerCase())) {
				output.setPrefName(input.getUniqueId());
			}
				
			output.setSystematicNames(null);
			Set<String> sysNames = input.getSystematicNames();
			if (Utils.hasContent(sysNames))
			{
				Iterator<String> iter = sysNames.iterator();
				String oneSysName = null;
				while (iter.hasNext()){
					oneSysName = iter.next();
					if (Utils.hasContent(oneSysName) && !namesTobeBlocked.contains(oneSysName.toLowerCase())) {
						output.addSystematicName(oneSysName);
					}
				}
			}
					
			output.setClassification(null);
			Set<String> classificationCodes = input.getClassification();
			if (Utils.hasContent(classificationCodes))
			{
				Iterator<String> iter = classificationCodes.iterator();
				String oneClassification = null;
				while (iter.hasNext()){
					oneClassification = iter.next();
					if (Utils.hasContent(oneClassification) && !classificationCodesTobeBlocked.contains(oneClassification.toLowerCase())) {
						output.addClassification(oneClassification);
					}
				}
			}
			return output;
		}
		/** clean up classification codes */
	
		
		private Set<String> getNamesToBeBlocked() throws IOException {
			Set<String> namesToBeBlocked = new HashSet<String>();
			String nameToBeBlocked = "INDEX NAME NOT YET ASSIGNED";
			namesToBeBlocked.add(nameToBeBlocked.toLowerCase());
			return namesToBeBlocked;
		}
			
	
	    private Set<String> getCodesToBeBlocked() throws IOException {
	
	    	String excelFilePath = "./data/CleanUpData/Classification codes to be eliminated_final V1.0.xlsx";
	    	FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
	         
	        Workbook workbook = new XSSFWorkbook(inputStream);
	        Sheet firstSheet = workbook.getSheetAt(0);
	        
	        // get the column indices to be used 
	        Row firstrow = firstSheet.getRow(0);
	        Iterator<Cell> firstRowCellIterator = firstrow.cellIterator();
	        Cell tempFirstRowCell = null;
	        String cellValue = null;
	        int collIndexText = -1;
	        int collIndexAction = -1;
	        
	        while (firstRowCellIterator.hasNext()) {
	        	tempFirstRowCell = firstRowCellIterator.next();
	        	cellValue = tempFirstRowCell.getStringCellValue();
	        	if (Utils.hasContent(cellValue) && cellValue.equalsIgnoreCase("Classification Code"))
	        		collIndexText = tempFirstRowCell.getColumnIndex();
	        	else if (Utils.hasContent(cellValue) && cellValue.equalsIgnoreCase("Action"))
	        		collIndexAction = tempFirstRowCell.getColumnIndex();
	        }
	        
	        Set<String> textsToBeBlocked = new HashSet<String>();
	        // If column indices are found, iterate over row and build a list of texts to be blocked
	        if ((collIndexText != -1) && (collIndexAction != -1))
	        {
		        Iterator<Row> iterator = firstSheet.iterator();
		        Row row = iterator.next(); // skip first row
		        Cell actionCell = null;
		        Cell textCell = null;
		        String actionValue = null;
		        String textValue = null;
		        while (iterator.hasNext()) {
		        	row = iterator.next();
		        	System.out.println("Processing row: " + row.getRowNum());
		        	if (row != null){
			            actionCell = row.getCell(collIndexAction);
			            if (actionCell != null) {
				            actionValue = actionCell.getStringCellValue();
				            if (Utils.hasContent(actionValue) && actionValue.equalsIgnoreCase("block")){
				            	textCell = row.getCell(collIndexText);
					            if (textCell != null) {
				            		textValue = textCell.getStringCellValue();
						            if (Utils.hasContent(textValue))
						            	textsToBeBlocked.add(textValue.trim().toLowerCase());
				            	}
				            }
			            }
		        	}
		        }
		    }
	        
	        workbook.close();
	        inputStream.close();
	        
	        return textsToBeBlocked;
	    }
	    
	 
}
