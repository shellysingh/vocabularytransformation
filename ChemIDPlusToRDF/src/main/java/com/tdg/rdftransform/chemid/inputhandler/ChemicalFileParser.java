package com.tdg.rdftransform.chemid.inputhandler;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.tdg.rdftransform.chemid.rdfhandler.RDFSerializationType;

public class ChemicalFileParser {
	
	private String inXMLFile;
	private String ourRDFFile;
	private RDFSerializationType outFormat;
	
	public ChemicalFileParser( String in, String out, RDFSerializationType format){
		
		inXMLFile = in;
		ourRDFFile = out;
		outFormat = format;
	}
	
	
	public void convertToRDF() throws Exception
	{ 
		SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        ChemicalDataHandler datahandler = new ChemicalDataHandler();
        datahandler.setOutPath(ourRDFFile);
        datahandler.setOutFormat(outFormat);
        saxParser.parse(inXMLFile, datahandler);   
	}
	
	public static void main(String[] args){

	      try {	
//	    	File inputFile = new File("./data/TransformedXML/1Chemical.xml");
//	    	String outfile = "./data/RDF/1Chemical_new.ttl";
//		     RDFSerializationType outFormat = RDFSerializationType.Turtle;
	    	 
//		    File inputFile = new File("./data/TransformedXML/1DiffChemical.xml");
//		    String outfile = "./data/RDF/1DiffChemical.rdf";

//	        File inputFile = new File("./data/TransformedXML/output1.xml");
//	    	String outfile = "./data/RDF/SampleChemicals_new.ttl";
//	    	RDFSerializationType outFormat = RDFSerializationType.Turtle;

	    	 File inputFile = new File("./data/TransformedXML/AllChemicals.xml");
	    	 String outfile = "./data/RDF/AllChemicals_new.ttl";
		     RDFSerializationType outFormat = RDFSerializationType.Turtle;
	    	 
		     SAXParserFactory factory = SAXParserFactory.newInstance();
	         SAXParser saxParser = factory.newSAXParser();
	         ChemicalDataHandler datahandler = new ChemicalDataHandler();
	         datahandler.setOutPath(outfile);
	         datahandler.setOutFormat(outFormat);
	         saxParser.parse(inputFile, datahandler);     
	         
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }   
}
