package com.tdg.rdftransform.chemid.rdfhandler;

public enum EntityType {
	  Chemical {
	      public String toString() {
	          return "Chemical";
	      }
	  },
	  Formula {
	      public String toString() {
	          return "Formula";
	      }
	  },
	  Classification {
	      public String toString() {
	          return "Classification";
	      }
	  },
	  Mixture {
	      public String toString() {
	          return "Mixture";
	      }
	  },
	  RegistryNumber {
	      public String toString() {
	          return "RegistryNumber";
	      }
	  }
}