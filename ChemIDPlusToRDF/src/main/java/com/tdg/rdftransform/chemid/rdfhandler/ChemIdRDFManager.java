package com.tdg.rdftransform.chemid.rdfhandler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import com.tdg.rdftransform.chemid.common.Utils;
import com.tdg.rdftransform.chemid.inputhandler.ChemicalData;

public class ChemIdRDFManager {
	
	private static ChemIdRDFManager instance;
	private static Model model; 
	
	private static void initialize()
	{
		instance = new ChemIdRDFManager();
		model = ModelFactory.createDefaultModel();
		model.setNsPrefix( "tdgchem", URIGenerator.Namespace.tdg.toString());
	}
	
	public static ChemIdRDFManager getInstance(){
		if (instance == null) 
			initialize();
		return instance;
	}
	

//	public void generateClassificationCodes()
//	{
//		String fileName = "./data/ClassificationCodes.csv";
//		FileWriter out = null;
//		try {
//			out = new FileWriter(fileName);
//			for (String s:allClassificationCodes)
//			{
//				out.write(s);
//				out.write("\n");
//			}
//			out.close();
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
	
	public void generateRDF(String outFilePath, RDFSerializationType outFormat) {
//		generateClassificationCodes();
		String fileName = outFilePath;
		FileWriter out = null;
		String format = "RDF/XML";
		try {
			if (outFormat!= null) 
				format = outFormat.toString();
			out = new FileWriter(fileName);
		    model.write( out, format);
		}
		catch(Exception io)
		{
			io.printStackTrace();
		}
		finally {
		   try {
		       out.close();
		   }
		   catch (IOException closeException) {
		       // ignore
		   }
		}
	}
	
	public void addChemical(ChemicalData chemical)
	{
		ChemicalData tempChem = chemical;
		System.out.println("Invoked: " +  tempChem.getUniqueId() + tempChem.getPrefName());

		String prefName = null;
		Set<String> tempValues = null;
		
		if ( Utils.hasContent(tempChem.getPrefName())){
			prefName = Utils.getCleanName(tempChem.getPrefName());
		}
		
		
		String uid = tempChem.getUniqueId();
		String uri = URIGenerator.getEntityUID(EntityType.Chemical, uid);
		
		Resource rChemical = model.createResource(uri);
		
		addAsSKOSConcept(rChemical);
		
		addCustomProperty(rChemical, "UniqueId", tempChem.getUniqueId());
		if ( Utils.hasContent(tempChem.getPrefFormula()))
			addCustomProperty(rChemical, "prefFormula", tempChem.getPrefFormula().trim());
//			addCustomPropertyAsEntity(rChemical, "prefFormula", EntityType.Formula, tempChem.getPrefFormula().trim());
		
		if ( Utils.hasContent(tempChem.getPrefName()))
			addCustomProperty(rChemical, "prefLabel", prefName);
		
		if ( Utils.hasContent(tempChem.getSystematicNames())){
			tempValues = Utils.getNonNullValueSet(tempChem.getSystematicNames());
			addCustomProperty(rChemical, "systematicName", tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getSynonyms())){
			tempValues = Utils.getNonNullValueSet(tempChem.getSynonyms());
			addCustomProperty(rChemical, "altLabel", tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getRegistryNumbers())){
			tempValues = Utils.getNonNullValueSet(tempChem.getRegistryNumbers());
			addCustomProperty(rChemical, "registryNumber", tempValues);
//			addCustomPropertyAsEntity(rChemical, "registryNumber", EntityType.RegistryNumber, tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getRelatedRegistryNumbers())){
			tempValues = Utils.getNonNullValueSet(tempChem.getRelatedRegistryNumbers());
//			addCustomPropertyAsEntity(rChemical, "relatedRegistryNumber", EntityType.RegistryNumber, tempValues);
			addCustomProperty(rChemical, "relatedRegistryNumber", tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getRelatedNames())){
			tempValues = Utils.getNonNullValueSet(tempChem.getRelatedNames());
			addCustomProperty(rChemical, "relatedName", tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getFormula())){
			tempValues = Utils.getNonNullValueSet(tempChem.getFormula());
			addCustomProperty(rChemical, "formula", tempValues);
//			addCustomPropertyAsEntity(rChemical, "formula", EntityType.Formula, tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getClassification())){
			tempValues = Utils.getNonNullValueSet(tempChem.getClassification());
			addCustomProperty(rChemical, "classification", tempValues);
//			addToSet(tempValues);		
//			addCustomPropertyAsEntity(rChemical, "classification", EntityType.Classification, tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getContainingMixtures())){
			tempValues = Utils.getNonNullValueSet(tempChem.getContainingMixtures());
			addCustomProperty(rChemical, "containingMixture", tempValues);
//			addCustomPropertyAsEntity(rChemical, "containingMixture", EntityType.Mixture, tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getFormulaFragmants())){
			tempValues = Utils.getNonNullValueSet(tempChem.getFormulaFragmants());
			addCustomProperty(rChemical, "formulaFragmant", tempValues);
//			addCustomPropertyAsEntity(rChemical, "formulaFragmant", EntityType.Formula, tempValues);
		}
		
		if ( Utils.hasContent(tempChem.getNote()))
			addCustomProperty(rChemical, "note", tempChem.getNote().trim());
		
	}
	
//	private static Set<String> allClassificationCodes = new HashSet<String>();
	
//	private void addToSet(Set<String> tempValues) {
//		allClassificationCodes.addAll(tempValues);
//	}

	private void addAsSKOSConcept(Resource rChemical) {
		
		//TODO: create statement for Resource RDFSTYPE skosConcept
//		//		resource.addProperty(RDF.type, "");
//		RDFNode o = model.createResource("");
//		Statement skosConceptStatement  = model.createStatement(rChemical,RDF.type,"http://www.w3.org/2004/02/skos/core#Concept");
		
//		RDFNode o = model.createResource( );
//		
		model.add(rChemical,RDF.type,URIGenerator.Namespace.skos + "Concept");
	}

	private void addCustomProperty(Resource resource, String propertyName, String propValue){
		
		// Clear data present in Square brackets
		propValue = Utils.getCleanName(propValue);
		if (propertyName.equalsIgnoreCase("registryNumber") || propertyName.equalsIgnoreCase("relatedRegistryNumber") )
		{
			propValue = Utils.getCleanRegistryNumber(propValue);
		}

		Property p = model.createProperty(URIGenerator.getNamespaceForPredicate(propertyName), propertyName);
		resource.addProperty(p, propValue);
		
		//TODO: change 
//		resource.addProperty(RDF.type, "");
//		resource.addProperty(SKOS., "");
//		resource.addProperty(RDFS., "");
	}
	
	private void addCustomProperty(Resource resource, String propertyName, Set<String> propValue){
		
		Property p = null;
		if (propValue!= null && propValue.size() > 0)
		{
			for(String name:propValue){
				addCustomProperty(resource, propertyName, name);		
			}
		}
	}

/*	private void addCustomPropertyAsEntity(Resource resource, String predicate, EntityType objectType, String objectNameValue){

		if (predicate.equalsIgnoreCase("registryNumber") || predicate.equalsIgnoreCase("relatedRegistryNumber") )
		{
			objectNameValue = Utils.getCleanRegistryNumber(objectNameValue);
		}
		
		Resource rTemp = model.createResource();
		
		Property p = model.createProperty(URIGenerator.getNamespaceForPredicate(predicate), predicate);	
		resource.addProperty(p, rTemp);
		
		p = model.createProperty(URIGenerator.getNamespaceForPredicate("type"), "prefLabel");	
		rTemp.addProperty(p, objectNameValue);
		
		p = model.createProperty(URIGenerator.getNamespaceForPredicate("type"),"type");	
		rTemp.addProperty(p, ""+ objectType);
		
	}
	
	private void addCustomPropertyAsEntity(Resource resource, String predicate, EntityType objectType, Set<String> objectNameValue){
		
		if (objectNameValue!= null && objectNameValue.size() > 0)
		{
			for(String name:objectNameValue){
				addCustomPropertyAsEntity(resource, predicate, objectType, name);		
			}
		}
	}
*/	
	public static void main(String []aa)
	{
		ChemIdRDFManager i = ChemIdRDFManager.getInstance();
	}

}
