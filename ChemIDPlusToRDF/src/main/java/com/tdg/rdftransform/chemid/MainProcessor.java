package com.tdg.rdftransform.chemid;

import com.tdg.rdftransform.chemid.inputhandler.ChemicalFileParser;
import com.tdg.rdftransform.chemid.rdfhandler.RDFSerializationType;
import com.tdg.rdftransform.chemid.xsltranform.XMLTransformerUsingXSL;

public class MainProcessor {

	public static void main(String [] aa) throws Exception
	{
		System.out.println("Execute the portions that you need.");
		
		String inputFile;
		String xsltFile;
		String intermediateXMLFile;
		String outputRDFFile;
		
		xsltFile = "./data/Transformer/TransformChemId_2.xslt";
		RDFSerializationType outFormat = RDFSerializationType.Turtle;

		//for one chemical
//		inputFile = "./data/NativeXML/InputWithOneChem.xml";
//		intermediateXMLFile = "./data/TransformedXML/IntermediateWithOneChem.xml";
//		outputRDFFile = "./data/RDF/RDFWithOneChem.ttl";
		
		//for sample
		inputFile = "./data/NativeXML/ChemIDPlus-Sample.xml";
		intermediateXMLFile = "./data/TransformedXML/IntermediateWithSample.xml";
		outputRDFFile = "./data/RDF/RDFWithSample.ttl";
		
		//for full chemidplus
//		inputFile = "./data/NativeXML/CurrentChemID.xml";
//		intermediateXMLFile = "./data/TransformedXML/IntermediateWithAllChemicals.xml";
//		outputRDFFile = "./data/RDF/RDFWithAllChemicals.ttl";
		
		
		// call for transformation to intermediate XML
		XMLTransformerUsingXSL xsltXformer = new XMLTransformerUsingXSL(inputFile,intermediateXMLFile,xsltFile);
		xsltXformer.transform();
		System.out.println("XSL based transformation complete");
		
		// call for transformation to RDF
		ChemicalFileParser rdfXformer = new ChemicalFileParser( intermediateXMLFile, outputRDFFile, outFormat);
		rdfXformer.convertToRDF();
		System.out.println("RDF creation complete");
		
	}
}
