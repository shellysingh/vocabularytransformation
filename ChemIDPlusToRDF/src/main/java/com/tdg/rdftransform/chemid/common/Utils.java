package com.tdg.rdftransform.chemid.common;

import java.util.HashSet;
import java.util.Set;

public class Utils {

	public static boolean hasContent(String aa)
	{
		boolean exists = false;
		if (aa != null && !aa.trim().equals(""))
			exists = true;
		return exists;
	}
	
	//Dexamethasone [BAN:INN:JAN]
	/** remove the data in square brackets */
	public static String getCleanName(String aa)
	{
		String cleanName = "";
		if (hasContent(aa)){
			cleanName= aa.replaceAll("\\[(.*?)\\]", "");
		}
		cleanName = cleanName.trim();
		return cleanName;
	}
	
	
	//518-26-3 (iodide.hydriodide)
	public static String getCleanRegistryNumber(String aa)
	{
		String cleanName = "";
		if (hasContent(aa)){
			cleanName= aa.replaceAll("\\((.*?)\\)", "");
		}
		cleanName = cleanName.trim();
		return cleanName;
	}
	
	public static boolean hasContent(Set<String> aa)
	{
		boolean exists = false;
		if (aa != null && aa.size() > 0)
		{
			for (String element:aa)
			{
				if (hasContent(element)){
					exists = true;
					break;
				}
			}
		}
		return exists;
	}
	
	public static Set<String> getNonNullValueSet(Set<String> aa)
	{
		Set<String> nonNullValueSet = new HashSet<String>();
		if (hasContent(aa))
		{
			for (String element:aa)
			{
				if (hasContent(element)){
					nonNullValueSet.add(element.trim());
				}
			}
		}
		return nonNullValueSet;
	}
	
	public static void main(String aa[])
	{
		String name = "Adiphenine hydrochloride [USAN]";
		System.out.println(":" + getCleanName(name) + ":");
	
		name = "Dexamethasone ";
		System.out.println(":" + getCleanName(name) + ":");
		
		name = "Dexamethasone [BAN:JAN]";
		System.out.println(":" + getCleanName(name) + ":");

		name = "518-26-3";
		System.out.println(":" + getCleanRegistryNumber(name) + ":");

		name = "518-26-3 ()";
		System.out.println(":" + getCleanRegistryNumber(name) + ":");

		name = "518-26-3 (iodide.hydriodide:hfuehfe)";
		System.out.println(":" + getCleanRegistryNumber(name) + ":");

		name = "518-26-3 (iodide.hydriodide)";
		System.out.println(":" + getCleanRegistryNumber(name) + ":");
	}
	
	
}
