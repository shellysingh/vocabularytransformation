package com.tdg.rdftransform.chemid.xsltranform;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class XMLTransformerUsingXSL {
	
	
	private String inFile;
	private String outFile;
	private String xslFile;
	
	
	public XMLTransformerUsingXSL(String inPath, String outPath, String XSLpath){
		this.inFile = inPath;
		this.outFile = outPath;
		this.xslFile= XSLpath;
	}
	
	public void transform() throws IOException, URISyntaxException, TransformerException 
	{
		TransformerFactory factory = TransformerFactory.newInstance();
		Source xslt = new StreamSource(new File(xslFile));
		Transformer transformer = factory.newTransformer(xslt);

		Source text = new StreamSource(new File(inFile));
		transformer.transform(text, new StreamResult(new File(outFile)));
		System.out.println("Transformation Done");
	}
	
	public static void main(String[] args) throws IOException,
			URISyntaxException, TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Source xslt = new StreamSource(new File("./data/TransformChemId_2.xslt"));
		Transformer transformer = factory.newTransformer(xslt);

//		Source text = new StreamSource(new File("./data/InputChemId.xml"));
//		transformer.transform(text, new StreamResult(new File("./output/1Chemical.xml")));

//		Source text = new StreamSource(new File("./data/ChemIDPlus-Sample.xml"));
//		transformer.transform(text, new StreamResult(new File("./output/SampleOutput.xml")));
		Source text = new StreamSource(new File("D:\\work\\ProductManagement\\Vocabulary\\ChemIdPlus - Dictionary\\CurrentChemID.xml"));
		transformer.transform(text, new StreamResult(new File("./output/AllChemicals.xml")));
		System.out.println("Done");
	}
}
