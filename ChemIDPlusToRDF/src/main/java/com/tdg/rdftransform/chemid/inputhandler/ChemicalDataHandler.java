package com.tdg.rdftransform.chemid.inputhandler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tdg.rdftransform.chemid.cleanup.DataCleaner;
import com.tdg.rdftransform.chemid.rdfhandler.ChemIdRDFManager;
import com.tdg.rdftransform.chemid.rdfhandler.RDFSerializationType;

public class ChemicalDataHandler extends DefaultHandler {

	private boolean bChemical;
	private boolean bUniqueId;
	private boolean bPrefFormula;
	private boolean bPrefName;
	private boolean bSystematicName;
	private boolean bSynonym;
	private boolean bRegistryNumber;
	private boolean bRelatedRegistryNumber;
	private boolean bRelatedName;
	private boolean bFormula;
	private boolean bClassification;
	private boolean bContainingMixture;
	private boolean bFormulaFragmant;
	private boolean bNote;
	private ChemIdRDFManager rdfMan ;
	private String outFilePath;
	private RDFSerializationType outFormat;
	private DataCleaner cleaner;
	
	
	ChemicalData tempChemical = null;
	StringBuilder sbTagValue = null;
	
	public void setOutPath(String path)
	{
		outFilePath = path;
	}
	
	public void setOutFormat(RDFSerializationType format)
	{
		outFormat = format;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		if (qName.equalsIgnoreCase("ChemicalList")) {
			rdfMan = ChemIdRDFManager.getInstance();
			try{ 
				cleaner = DataCleaner.getInstance();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		if (qName.equalsIgnoreCase("Chemical")) {
			tempChemical = new ChemicalData();
		} else if (qName.equalsIgnoreCase("UniqueId")) {
			sbTagValue = new StringBuilder();
			bUniqueId = true;
		} else if (qName.equalsIgnoreCase("PrefFormula")) {
			sbTagValue = new StringBuilder();
			bPrefFormula = true;
		} else if (qName.equalsIgnoreCase("PrefName")) {
			sbTagValue = new StringBuilder();
			bPrefName = true;
		} else if (qName.equalsIgnoreCase("Synonym")) {
			sbTagValue = new StringBuilder();
			bSynonym = true;
		} else if (qName.equalsIgnoreCase("SystematicName")) {
			sbTagValue = new StringBuilder();
			bSystematicName = true;
		} else if (qName.equalsIgnoreCase("RelatedName")) {
			sbTagValue = new StringBuilder();
			bRelatedName = true;
		} else if (qName.equalsIgnoreCase("RegistryNumber")) {
			sbTagValue = new StringBuilder();
			bRegistryNumber = true;
		} else if (qName.equalsIgnoreCase("RelatedRegistryNumber")) {
			sbTagValue = new StringBuilder();
			bRelatedRegistryNumber = true;
		} else if (qName.equalsIgnoreCase("ContainingMixture")) {
			sbTagValue = new StringBuilder();
			bContainingMixture = true;
		} else if (qName.equalsIgnoreCase("Formula")) {
			sbTagValue = new StringBuilder();
			bFormula = true;
		} else if (qName.equalsIgnoreCase("FormulaFragmant")) {
			sbTagValue = new StringBuilder();
			bFormulaFragmant = true;
		} else if (qName.equalsIgnoreCase("Classification")) {
			sbTagValue = new StringBuilder();
			bClassification = true;
		} else if (qName.equalsIgnoreCase("Note")) {
			sbTagValue = new StringBuilder();
			bNote = true;
		}
		
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		String content  = "";
		if (sbTagValue != null ) 
			content = sbTagValue.toString();
		
		if (qName.equalsIgnoreCase("ChemicalList")) {
			System.out.println("Finished whole XML");
			rdfMan.generateRDF(outFilePath, outFormat);
			
		} 
		else if (qName.equalsIgnoreCase("Chemical")) {
			System.out.println("Invoke Cleanup of chemical");
			
			if (cleaner != null)
				tempChemical = cleaner.getCleanedUpData(tempChemical);
			
			System.out.println("Invoke RDF at this point");
			rdfMan.addChemical(tempChemical);
		}
		else if (bUniqueId) {
			tempChemical.setUniqueId (content);
			bUniqueId = false;
		}
		else if (bPrefFormula) {
			tempChemical.setPrefFormula (content);
			bPrefFormula = false;
		}
		else if (bPrefName) {
			tempChemical.setPrefName(content);
			bPrefName = false;
		}
		else if (bSystematicName) {
			tempChemical.addSystematicName(content);
			bSystematicName = false;
		}
		else if (bSynonym) {
			tempChemical.addSynonym(content);
			bSynonym = false;
		}
		else if (bRegistryNumber) {
			tempChemical.addRegistryNumber(content);
			bRegistryNumber = false;
		}
		else if (bRelatedRegistryNumber) {
			tempChemical.addRelatedRegistryNumber(content);
			bRelatedRegistryNumber = false;
		}
		else if (bRelatedName) {
			tempChemical.addRelatedName(content);
			bRelatedName = false;
		}
		else if (bFormula) {
			tempChemical.addFormula(content);
			bFormula = false;
		}
		else if (bClassification) {
			tempChemical.addClassification(content);
			bClassification = false;
		}
		else if (bContainingMixture) {
			tempChemical.addContainingMixture(content);
			bContainingMixture = false;
		}
		else if (bFormulaFragmant) {
			tempChemical.addFormulaFragmant(content);
			bFormulaFragmant = false;
		}
		else if (bNote) {
			tempChemical.setNote(content);
			bNote = false;
		}
		
	}

	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException {
		
		String content = new String(ch, start, length);
		if (sbTagValue!= null) 
		{
			sbTagValue.append(content);
		}
	
	}
}
