package com.tdg.rdftransform.chemid.rdfhandler;

public enum RDFSerializationType {
	  Turtle {
	      public String toString() {
	          return "Turtle";
	      }
	  },
	  RDFXML {
	      public String toString() {
	          return "Formula";
	      }
	  },
	  N3 {
	      public String toString() {
	          return "Classification";
	      }
	  }
}