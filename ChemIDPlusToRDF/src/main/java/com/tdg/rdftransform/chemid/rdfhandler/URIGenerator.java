package com.tdg.rdftransform.chemid.rdfhandler;

import com.tdg.rdftransform.chemid.rdfhandler.EntityType;

public class URIGenerator {
	
	private static String ns = "https://www.tdg.com/ChemIdPlus/";
	
	public static enum Namespace {
		tdg {
		      public String toString() {
		    	  //TODO: change here
//		          return "https://www.tdg.com/ChemIdPlus/";
		          return "http://www.tdg.com/2017/ChemIdPlus#";
		      }
		  },
		  skos {
		      public String toString() {
		          return "http://www.w3.org/2004/02/skos/core#";
		      }
		  },
		  rdfs {
		      public String toString() {
		          return "http://www.w3.org/2000/01/rdf-schema#";
		      }
		  },
		  rdf{
			  public String toString() {
					return  "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
			  }
		  }
	}

	public static String getEntityUID(EntityType type, String name)
	{
		String uid =  "";
		uid = ns + type + "#"+ name;
		return uid;
	}
	
	public static String getPredicateUID(String name)
	{
		String uid =  "";
		uid = ns + name;
		return uid;
	}
	
	public static String getPredicateUID(Namespace namespace, String name)
	{
		String uid =  "";
		uid = ns + name;
		return uid;
	}
	
	public static String getNamespaceForPredicate(String predicate)
	{
		URIGenerator.Namespace ns = URIGenerator.Namespace.tdg; 
		if (predicate.equalsIgnoreCase("prefLabel") || predicate.equalsIgnoreCase("altLabel") || predicate.equalsIgnoreCase("Concept") || predicate.equalsIgnoreCase("note"))
			ns = URIGenerator.Namespace.skos;
		else if (predicate.equalsIgnoreCase("type"))	
				ns = URIGenerator.Namespace.rdf;
		return ns.toString();
	}
	
}
