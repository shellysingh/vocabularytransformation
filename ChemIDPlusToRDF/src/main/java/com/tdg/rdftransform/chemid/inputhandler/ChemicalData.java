package com.tdg.rdftransform.chemid.inputhandler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ChemicalData {

	private String uniqueId;
	private String prefFormula;
	private String prefName;
	private Set<String> systematicNames;
	private Set<String> synonyms;
	private Set<String> registryNumbers;
	private Set<String> relatedRegistryNumbers;
	private Set<String> relatedNames;
	private Set<String> formula;
	private Set<String> classification;
	private Set<String> containingMixtures;
	private Set<String> formulaFragmants;
	private String note;
	
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public String getPrefFormula() {
		return prefFormula;
	}
	
	public void setPrefFormula(String prefFormula) {
		this.prefFormula = prefFormula;
	}
	
	public String getPrefName() {
		return prefName;
	}
	public void setPrefName(String prefName) {
		this.prefName = prefName;
	}
	
	public Set<String> getSystematicNames() {
		return systematicNames;
	}
	public void setSystematicNames(Set<String> systematicNames) {
		this.systematicNames = systematicNames;
	}
	public void addSystematicName(String systematicName) {
		if ( this.systematicNames == null)
			this.systematicNames = new HashSet<String>();
		if (!this.systematicNames.contains(systematicName))
		this.systematicNames.add(systematicName);
	}

	public Set<String> getSynonyms() {
		return synonyms;
	}
	public void setSynonyms(Set<String> synonyms) {
		this.synonyms = synonyms;
	}
	public void addSynonym(String Synonym) {
		if ( this.synonyms == null)
			this.synonyms = new HashSet<String>();
		if (!this.synonyms.contains(Synonym))
		this.synonyms.add(Synonym);
	}
	
	public Set<String> getRegistryNumbers() {
		return registryNumbers;
	}
	public void setRegistryNumbers(Set<String> registryNumbers) {
		this.registryNumbers = registryNumbers;
	}
	public void addRegistryNumber(String registryNumber) {
		if ( this.registryNumbers == null)
			this.registryNumbers = new HashSet<String>();
		if (!this.registryNumbers.contains(registryNumber))
		this.registryNumbers.add(registryNumber);
	}
	
	public Set<String> getRelatedRegistryNumbers() {
		return relatedRegistryNumbers;
	}
	public void setRelatedRegistryNumbers(Set<String> relatedRegistryNumbers) {
		this.relatedRegistryNumbers = relatedRegistryNumbers;
	}
	public void addRelatedRegistryNumber(String relatedRegistryNumber) {
		if ( this.relatedRegistryNumbers == null)
			this.relatedRegistryNumbers = new HashSet<String>();
		if (!this.relatedRegistryNumbers.contains(relatedRegistryNumber))
		this.relatedRegistryNumbers.add(relatedRegistryNumber);
	}
	
	public Set<String> getRelatedNames() {
		return relatedNames;
	}
	public void setRelatedNames(Set<String> relatedNames) {
		this.relatedNames = relatedNames;
	}
	public void addRelatedName(String relatedName) {
		if ( this.relatedNames == null)
			this.relatedNames = new HashSet<String>();
		if (!this.relatedNames.contains(relatedName))
		this.relatedNames.add(relatedName);
	}
	
	public Set<String> getFormula() {
		return formula;
	}
	public void setFormula(Set<String> formula) {
		this.formula = formula;
	}
	public void addFormula(String formula) {
		if ( this.formula == null)
			this.formula = new HashSet<String>();
		if (!this.formula.contains(formula))
		this.formula.add(formula);
	}
	
	public Set<String> getClassification() {
		return classification;
	}
	public void setClassification(Set<String> classification) {
		this.classification = classification;
	}
	public void addClassification(String classification) {
		if ( this.classification == null)
			this.classification = new HashSet<String>();
		if (!this.classification.contains(classification))
		this.classification.add(classification);
	}
	
	public Set<String> getContainingMixtures() {
		return containingMixtures;
	}
	public void setContainingMixtures(Set<String> containingMixtures) {
		this.containingMixtures = containingMixtures;
	}
	public void addContainingMixture(String containingMixture) {
		if ( this.containingMixtures == null)
			this.containingMixtures = new HashSet<String>();
		if (!this.containingMixtures.contains(containingMixture))
		this.containingMixtures.add(containingMixture);
	}
	
	public Set<String> getFormulaFragmants() {
		return formulaFragmants;
	}
	public void setFormulaFragmants(Set<String> formulaFragmants) {
		this.formulaFragmants = formulaFragmants;
	}
	public void addFormulaFragmant(String formulaFragmant) {
		if ( this.formulaFragmants == null)
			this.formulaFragmants = new HashSet<String>();
		if (!this.formulaFragmants.contains(formulaFragmant))
		this.formulaFragmants.add(formulaFragmant);
	}
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	public ChemicalData() {
		super();
	}
	
	public ChemicalData(ChemicalData copyFrom) {
		new ChemicalData();
		setUniqueId(copyFrom.getUniqueId());
		setPrefFormula(copyFrom.getPrefFormula());
		setPrefName(copyFrom.getPrefName());
		setSystematicNames(copyFrom.getSystematicNames());
		setSynonyms(copyFrom.getSynonyms());
		setRegistryNumbers(copyFrom.getRegistryNumbers());
		setRelatedRegistryNumbers(copyFrom.getRelatedRegistryNumbers());
		setRelatedNames(copyFrom.getRelatedNames());
		setFormula(copyFrom.getFormula());
		setClassification(copyFrom.getClassification());
		setContainingMixtures(copyFrom.getContainingMixtures());
		setFormulaFragmants(copyFrom.getContainingMixtures());
		setNote(copyFrom.getNote());
	}

	@Override
	public String toString() {
		return "ChemicalData [uniqueId=" + uniqueId + ", prefFormula="
				+ prefFormula + ", prefName=" + prefName + ", systematicNames="
				+ systematicNames + ", synonyms=" + synonyms
				+ ", registryNumbers=" + registryNumbers
				+ ", relatedRegistryNumbers=" + relatedRegistryNumbers
				+ ", relatedNames=" + relatedNames + ", formula=" + formula
				+ ", classification=" + classification
				+ ", containingMixtures=" + containingMixtures
				+ ", formulaFragmants=" + formulaFragmants + ", note=" + note
				+ "]";
	}
	
	
}
