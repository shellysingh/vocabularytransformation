<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<!--     <xsl:template match="/file/Chemical">
 -->    <xsl:template match="/file">
 <xsl:text>&#xa;</xsl:text>
 <ChemicalList>
 		<xsl:text>&#xa;</xsl:text>
	 <xsl:for-each select="/file/Chemical">
        <xsl:variable name="var_id" select="@id"/>
        <xsl:variable name="var_displayFormula" select="@displayFormula"/>
        <xsl:variable name="var_displayName" select="@displayName"/>
    	<Chemical>
    		<UniqueId><xsl:value-of select="$var_id" /></UniqueId>
    		<PrefFormula><xsl:value-of select="$var_displayFormula"/></PrefFormula>
    		<PrefName><xsl:value-of select="$var_displayName"/></PrefName>
    		
	    	<!-- Transform to Synonym -->
	    	<!-- NameList/NameOfSubstance
	    	NameList/Synonyms
	    	NameList/DescriptorName
	     	-->
	    	<xsl:for-each select="NameList/NameOfSubstance | NameList/Synonyms | NameList/DescriptorName">
	    	 		<Synonym><xsl:value-of select="normalize-space(./text())"/></Synonym>
			</xsl:for-each> 
	
	     	<!-- Transform to SystematicName -->
	    	<xsl:for-each select="NameList/SystematicName">
	    		<SystematicName><xsl:value-of select="normalize-space(./text())"/></SystematicName>
			</xsl:for-each> 
    	    
   	     	<!-- Transform to RelatedName -->
	    	<xsl:for-each select="NameList/SuperListName">
	    		<RelatedName><xsl:value-of select="normalize-space(./text())"/></RelatedName>
			</xsl:for-each> 
    	       	     	
   	     	<!-- Transform to RegistrynNumber -->
    	    <xsl:for-each select="NumberList/CASRegistryNumber | NumberList/IdentificationNumber | NumberList/OtherRegistryNumber">
	    		<RegistryNumber><xsl:value-of select="normalize-space(./text())"/></RegistryNumber>
			</xsl:for-each> 
    	    <xsl:for-each select="NumberList/RelatedRegistryNumber">
	    		<RelatedRegistryNumber><xsl:value-of select="normalize-space(./text())"/></RelatedRegistryNumber>
			</xsl:for-each> 
			
   	     	<!-- Transform to Formula -->
    	    <xsl:for-each select="FormulaList/MolecularFormula">
	    		<Formula><xsl:value-of select="normalize-space(./text())"/></Formula>
			</xsl:for-each> 
			
			<xsl:for-each select="FormulaFragmentList/FormulaFragment">
	    		<FormulaFragmant><xsl:value-of select="normalize-space(./text())"/></FormulaFragmant>
			</xsl:for-each> 
	    	
	    	<!-- Transform to ContainingMixture -->
   	     	<xsl:for-each select="NameList/MixtureName">
	    		<ContainingMixture><xsl:value-of select="normalize-space(./text())"/></ContainingMixture>
			</xsl:for-each> 
						 	
    	     	
    	    <!-- Transform to Classification -->
    	    <xsl:for-each select="ClassificationList/ClassificationCode">
	    		<Classification><xsl:value-of select="normalize-space(./text())"/></Classification>
			</xsl:for-each> 
		
			<xsl:for-each select="ClassificationList/SuperlistClassCode">
	    		<Classification><xsl:value-of select="normalize-space(./text())"/></Classification>
			</xsl:for-each> 
			
			<!-- Transform to Note -->
			<xsl:for-each select="NoteList/Note">
	    		<Note><xsl:value-of select="normalize-space(./text())"/></Note>
			</xsl:for-each> 
			
			
			<!--  molecular weight -->
    	     	 	
    	</Chemical>
    	<xsl:text>&#xa;</xsl:text>
    	</xsl:for-each> 
			
    	</ChemicalList>
    </xsl:template>

</xsl:stylesheet>
